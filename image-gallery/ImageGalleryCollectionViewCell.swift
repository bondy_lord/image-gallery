//
//  ImageCollectionViewCell.swift
//  image-gallery
//
//  Created by Omer Rahmany on 11/08/2019.
//  Copyright © 2019 Omer Rahmany. All rights reserved.
//

import UIKit

class ImageGalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
