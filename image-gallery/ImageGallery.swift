//
//  ImageGallery.swift
//  image-gallery
//
//  Created by Omer Rahmany on 12/08/2019.
//  Copyright © 2019 Omer Rahmany. All rights reserved.
//

import Foundation

class ImageGallery {
    var images: [Image] = []
}

struct Image {
     var url: URL
     var aspectRatio: (heightRatio:Double,widthRatio:Double)
    
    init(url: URL, heightRatio: Double, widthRatio:Double) {
        self.url = url
        self.aspectRatio = (1.0, 1.0)
    }
}
